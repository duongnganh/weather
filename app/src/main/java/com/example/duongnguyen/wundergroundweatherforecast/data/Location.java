package com.example.duongnguyen.wundergroundweatherforecast.data;

import org.json.JSONObject;

/**
 * Created by Duong Nguyen on 3/27/2016.
 */
public class Location implements JSONPopulator {
    private String country_iso3166;
    private String city;
    private String lat;
    private String lon;

    public String getCountry_iso3166() {
        return country_iso3166;
    }

    public String getCity() {
        return city;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    @Override
    public void populate(JSONObject data) {
        country_iso3166 = data.optString("country_iso3166");
        city = data.optString("city");
        lat = data.optString("lat");
        lon = data.optString("lon");
    }
}
