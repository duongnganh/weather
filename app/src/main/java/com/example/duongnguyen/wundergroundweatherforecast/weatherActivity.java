package com.example.duongnguyen.wundergroundweatherforecast;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.duongnguyen.wundergroundweatherforecast.data.Current_observation;
import com.example.duongnguyen.wundergroundweatherforecast.data.Forecast;
import com.example.duongnguyen.wundergroundweatherforecast.data.Hourly;
//import com.example.duongnguyen.wundergroundweatherforecast.data.Location;
import com.example.duongnguyen.wundergroundweatherforecast.service.WeatherServiceCallback;
import com.example.duongnguyen.wundergroundweatherforecast.service.weatherService;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.io.InputStream;


import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class weatherActivity extends AppCompatActivity implements
        WeatherServiceCallback, ConnectionCallbacks, OnConnectionFailedListener, LocationListener{

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;

    private TextView temperatureTextView;
    private TextView conditionTextView;
    private TextView locationTextView;
    private TextView humidityTextView;
    private GifImageView weatherIconImageView;

    private TextView oneHour, twoHour, threeHour, fourHour, fiveHour;
    private ImageView iconOneHour, iconTwoHour, iconThreeHour, iconFourHour, iconFiveHour;
    private TextView tempOneHour, tempTwoHour, tempThreeHour, tempFourHour, tempFiveHour;

    private TextView tmr, nextDay;
    private ImageView iconToday, iconTmr, iconNextDay;
    private TextView highToday, highTmr, highNextDay;
    private TextView lowToday, lowTmr, lowNextDay;
    private TextView humToday, humTmr, humNextDay;

    private weatherService service;
    private ProgressDialog dialog; //for delay

    String lat, lon;
    final String DEGREE = "\u00b0";
    final String WATER = "\uD83D\uDCA7";

    final String url_header = "http://icons.wxug.com/i/c/k/";
    final String[] urls = {"clear.gif", "cloudy.gif", "flurries.gif", "hazy.gif", "mostlycloudy.gif", "mostlysunny.gif", "partlycloudy.gif", "partlysunny.gif", "sleet.gif", "rain.gif", "snow.gif", "sunny.gif", "tstorms.gif", "flurries.gif", "chancerain.gif", "chancesleet.gif", "chancesnow.gif", "chancetstorms.gif", "nt_clear.gif", "nt_cloudy.gif", "nt_flurries.gif", "nt_hazy.gif", "nt_mostlycloudy.gif", "nt_mostlysunny.gif", "nt_partlycloudy.gif", "nt_partlysunny.gif", "nt_sleet.gif", "nt_rain.gif", "nt_snow.gif", "nt_sunny.gif", "nt_tstorms.gif", "nt_chanceflurries.gif", "nt_chancerain.gif", "nt_chancesleet.gif", "nt_chancesnow.gif", "nt_chancetstorms.gif"};
    final int[] gifs = {R.drawable.clear, R.drawable.cloudy, R.drawable.flurries, R.drawable.hazy, R.drawable.mostlycloudy, R.drawable.mostlysunny, R.drawable.mostlysunny, R.drawable.mostlycloudy, R.drawable.sleet, R.drawable.rain, R.drawable.snow, R.drawable.clear, R.drawable.tstorms, R.drawable.flurries, R.drawable.rain, R.drawable.sleet, R.drawable.snow, R.drawable.tstorms, R.drawable.nt_clear, R.drawable.cloudy, R.drawable.flurries, R.drawable.hazy, R.drawable.nt_mostlycloudy, R.drawable.nt_mostlysunny, R.drawable.nt_mostlysunny, R.drawable.nt_mostlycloudy, R.drawable.sleet, R.drawable.rain, R.drawable.snow, R.drawable.nt_clear, R.drawable.tstorms, R.drawable.flurries, R.drawable.rain, R.drawable.sleet, R.drawable.snow, R.drawable.tstorms};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        if(mGoogleApiClient!= null){
            mGoogleApiClient.connect();
        }*/

        temperatureTextView = (TextView) findViewById(R.id.temperatureTextView);
        conditionTextView = (TextView) findViewById(R.id.conditionTextView);
        locationTextView = (TextView) findViewById(R.id.locationTextView);
        humidityTextView = (TextView) findViewById(R.id.humidityTextView);
        weatherIconImageView = (GifImageView) findViewById(R.id.weatherIconImageView);

        oneHour = (TextView) findViewById(R.id.oneHour);
        twoHour = (TextView) findViewById(R.id.twoHour);
        threeHour = (TextView) findViewById(R.id.threeHour);
        fourHour = (TextView) findViewById(R.id.fourHour);
        fiveHour = (TextView) findViewById(R.id.fiveHour);

        iconOneHour = (ImageView) findViewById(R.id.iconOneHour);
        iconTwoHour = (ImageView) findViewById(R.id.iconTwoHour);
        iconThreeHour = (ImageView) findViewById(R.id.iconThreeHour);
        iconFourHour = (ImageView) findViewById(R.id.iconFourHour);
        iconFiveHour = (ImageView) findViewById(R.id.iconFiveHour);

        tempOneHour = (TextView) findViewById(R.id.tempOneHour);
        tempTwoHour = (TextView) findViewById(R.id.tempTwoHour);
        tempThreeHour = (TextView) findViewById(R.id.tempThreeHour);
        tempFourHour = (TextView) findViewById(R.id.tempFourHour);
        tempFiveHour = (TextView) findViewById(R.id.tempFiveHour);

        tmr = (TextView) findViewById(R.id.tmr);
        nextDay = (TextView) findViewById(R.id.nextDay);

        iconToday = (ImageView) findViewById(R.id.iconToday);
        iconTmr = (ImageView) findViewById(R.id.iconTmr);
        iconNextDay = (ImageView) findViewById(R.id.iconNextDay);

        highToday = (TextView) findViewById(R.id.highToday);
        highTmr = (TextView) findViewById(R.id.highTmr);
        highNextDay = (TextView) findViewById(R.id.highNextDay);

        lowToday = (TextView) findViewById(R.id.lowToday);
        lowTmr = (TextView) findViewById(R.id.lowTmr);
        lowNextDay = (TextView) findViewById(R.id.lowNextDay);

        humToday = (TextView) findViewById(R.id.humToday);
        humTmr = (TextView) findViewById(R.id.humTmr);
        humNextDay = (TextView) findViewById(R.id.humNextDay);

        service = new weatherService(this);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        //service.refreshLocation();
	    //service.refreshConditions(lat, lon);

        final SwipeRefreshLayout swipeView = (SwipeRefreshLayout) findViewById(R.id.swipeView);
        swipeView.setColorSchemeColors(android.R.color.holo_blue_bright);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Loading");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        dialog.show();
                        mGoogleApiClient.reconnect();
                    }
                },1);
            }
        });
    }


    //@Override
    public void serviceGeolookupSuccess(Location location) {
        //locationTextView.setText(location.getCity());
        service.refreshConditions(lat, lon);
    }

    @Override
    public void serviceConditionsSuccess(Current_observation current_observation) {
        temperatureTextView.setText(current_observation.getTemp_c() + DEGREE + "C");
        conditionTextView.setText(current_observation.getWeather());
        humidityTextView.setText(current_observation.getRelative_humidity() + WATER);
        try {
            String url = current_observation.getIcon_url();
            int path;
            for (int i = 0; i < urls.length; i++) {
                if (url.equals(url_header + urls[i])) {
                    path = gifs[i];
                    GifDrawable weatherIconDrawable = new GifDrawable(getResources(), path);
                    weatherIconImageView.setImageDrawable(weatherIconDrawable);
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        service.refreshHourly(lat, lon);
    }

    @Override
    public void serviceHourlySuccess(Hourly hourly) {
        oneHour.setText(hourly.getHours()[0]);
        twoHour.setText(hourly.getHours()[1]);
        threeHour.setText(hourly.getHours()[2]);
        fourHour.setText(hourly.getHours()[3]);
        fiveHour.setText(hourly.getHours()[4]);

        (new DownloadImageWithURLTask(iconOneHour)).execute(hourly.getIcon_urls()[0]);
        (new DownloadImageWithURLTask(iconTwoHour)).execute(hourly.getIcon_urls()[1]);
        (new DownloadImageWithURLTask(iconThreeHour)).execute(hourly.getIcon_urls()[2]);
        (new DownloadImageWithURLTask(iconFourHour)).execute(hourly.getIcon_urls()[3]);
        (new DownloadImageWithURLTask(iconFiveHour)).execute(hourly.getIcon_urls()[4]);

        tempOneHour.setText(hourly.getTemps()[0] + DEGREE + "C");
        tempTwoHour.setText(hourly.getTemps()[1] + DEGREE + "C");
        tempThreeHour.setText(hourly.getTemps()[2] + DEGREE + "C");
        tempFourHour.setText(hourly.getTemps()[3] + DEGREE + "C");
        tempFiveHour.setText(hourly.getTemps()[4] + DEGREE + "C");

        service.refreshForecast(lat, lon);
    }

    @Override
    public void serviceForecastSuccess(Forecast forecast) {
        tmr.setText(forecast.getDays()[1]);
        nextDay.setText(forecast.getDays()[2]);

        (new DownloadImageWithURLTask(iconToday)).execute(forecast.getIcon_urls()[0]);
        (new DownloadImageWithURLTask(iconTmr)).execute(forecast.getIcon_urls()[1]);
        (new DownloadImageWithURLTask(iconNextDay)).execute(forecast.getIcon_urls()[2]);

        highToday.setText(forecast.getHighs()[0] + DEGREE + "C");
        highTmr.setText(forecast.getHighs()[1] + DEGREE + "C");
        highNextDay.setText(forecast.getHighs()[2] + DEGREE + "C");

        lowToday.setText(forecast.getLows()[0] + DEGREE + "C");
        lowTmr.setText(forecast.getLows()[1] + DEGREE + "C");
        lowNextDay.setText(forecast.getLows()[2] + DEGREE + "C");

        humToday.setText(WATER + forecast.getAvehumidity()[0] + "%");
        humTmr.setText(WATER + forecast.getAvehumidity()[1] + "%");
        humNextDay.setText(WATER + forecast.getAvehumidity()[2] + "%");
        dialog.hide();
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause(){
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Toast.makeText(this,"buildGoogleApiClient",Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this,"onConnected",Toast.LENGTH_SHORT).show();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setFastestInterval(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        //mLocationRequest.setSmallestDisplacement(0.1F);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this,"onConnectionSuspended",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this,"onConnectionFailed",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        //dialog.hide();
        temperatureTextView.setText("accuracy: " + location.getAccuracy() + " lat: " + location.getLatitude() + " lon: " + location.getLongitude());
        Toast.makeText(this,"Location Changed",Toast.LENGTH_SHORT).show();
        service.refreshConditions(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
    }

    private class DownloadImageWithURLTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageWithURLTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String pathToFile = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(pathToFile).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}


