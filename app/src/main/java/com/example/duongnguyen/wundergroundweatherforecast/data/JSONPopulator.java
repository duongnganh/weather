package com.example.duongnguyen.wundergroundweatherforecast.data;

import org.json.JSONObject;

/**
 * Created by Duong Nguyen on 3/27/2016.
 */
public interface JSONPopulator {
    void populate(JSONObject data);
}
