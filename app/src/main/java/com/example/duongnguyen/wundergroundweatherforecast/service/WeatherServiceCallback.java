package com.example.duongnguyen.wundergroundweatherforecast.service;

import com.example.duongnguyen.wundergroundweatherforecast.data.Current_observation;
import com.example.duongnguyen.wundergroundweatherforecast.data.Forecast;
import com.example.duongnguyen.wundergroundweatherforecast.data.Hourly;
import com.example.duongnguyen.wundergroundweatherforecast.data.Location;

/**
 * Created by Duong Nguyen on 3/27/2016.
 */
public interface WeatherServiceCallback {
    void serviceFailure(Exception exception);
    //void serviceGeolookupSuccess(Location location);
    void serviceConditionsSuccess(Current_observation current_observation);
    void serviceForecastSuccess(Forecast forecast);
    void serviceHourlySuccess(Hourly hourly);
}
