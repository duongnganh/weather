package com.example.duongnguyen.wundergroundweatherforecast.data;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Anh Duong on 28/03/2016.
 */
public class Hourly implements JSONArrayPopulator {
    private String[] hours = new String[5];
    private String[] temps = new String[5];
    private String[] icon_urls = new String[5];

    public String[] getHours() {
        return hours;
    }

    public String[] getTemps() {
        return temps;
    }

    public String[] getIcon_urls() {
        return icon_urls;
    }

    @Override
    public void populate(JSONArray dataArray) {
        JSONObject temporary;
        for (int i = 0; i < 5; i++){
            temporary = dataArray.optJSONObject(i).optJSONObject("FCTTIME");
            hours[i] = ((Integer.parseInt(temporary.optString("hour"))) % 12)+" "+temporary.optString("ampm");
            temps[i] = dataArray.optJSONObject(i).optJSONObject("temp").optString("metric");
            icon_urls[i] = dataArray.optJSONObject(i).optString("icon_url");
        }
    }
}
