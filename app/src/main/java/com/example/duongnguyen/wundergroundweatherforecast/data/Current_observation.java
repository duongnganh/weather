package com.example.duongnguyen.wundergroundweatherforecast.data;

import org.json.JSONObject;

/**
 * Created by Duong Nguyen on 3/27/2016.
 */
public class Current_observation implements JSONPopulator{
    private String weather;
    private int temp_f;
    private int temp_c;
    private String relative_humidity;
    private String icon_url;

    public String getWeather() {
        return weather;
    }

    public int getTemp_f() {
        return temp_f;
    }

    public int getTemp_c() {
        return temp_c;
    }

    public String getRelative_humidity() {
        return relative_humidity;
    }

    public String getIcon_url() {
        return icon_url;
    }

    @Override
    public void populate(JSONObject data) {
        weather = data.optString("weather");
        temp_f = data.optInt("temp_f");
        temp_c = data.optInt("temp_c");
        relative_humidity = data.optString("relative_humidity");
        icon_url = data.optString("icon_url");
    }
}
