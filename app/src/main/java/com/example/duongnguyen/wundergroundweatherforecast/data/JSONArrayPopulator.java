package com.example.duongnguyen.wundergroundweatherforecast.data;

import org.json.JSONArray;

/**
 * Created by Anh Duong on 28/03/2016.
 */
public interface JSONArrayPopulator {
    void populate(JSONArray dataArray);
}
