package com.example.duongnguyen.wundergroundweatherforecast.data;

import org.json.JSONArray;

/**
 * Created by Duong Nguyen on 3/27/2016.
 */
public class Forecast implements JSONArrayPopulator{
    private String[] days = new String[3];
    private String[] highs = new String[3];
    private String[] lows = new String[3];
    private String[] icon_urls = new String[3];
    private int[] avehumidity = new int[3];

    public String[] getDays() {
        return days;
    }

    public String[] getHighs() {
        return highs;
    }

    public String[] getLows() {
        return lows;
    }

    public String[] getIcon_urls() {
        return icon_urls;
    }

    public int[] getAvehumidity() {
        return avehumidity;
    }

    @Override
    public void populate(JSONArray dataArray) {
        for (int i = 0; i < 3; i++){
            days[i] = dataArray.optJSONObject(i).optJSONObject("date").optString("weekday_short");
            highs[i] = dataArray.optJSONObject(i).optJSONObject("high").optString("celsius");
            lows[i] = dataArray.optJSONObject(i).optJSONObject("low").optString("celsius");
            icon_urls[i] = dataArray.optJSONObject(i).optString("icon_url");
            avehumidity[i] = dataArray.optJSONObject(i).optInt("avehumidity");
        }

    }
}
