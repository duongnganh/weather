package com.example.duongnguyen.wundergroundweatherforecast.service;

import android.os.AsyncTask;

import com.example.duongnguyen.wundergroundweatherforecast.data.Current_observation;
import com.example.duongnguyen.wundergroundweatherforecast.data.Forecast;
import com.example.duongnguyen.wundergroundweatherforecast.data.Hourly;
import com.example.duongnguyen.wundergroundweatherforecast.data.Location;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Duong Nguyen on 3/27/2016.
 */
public class weatherService {
    private WeatherServiceCallback callback;
    private Location location;
    private Exception error;

    public weatherService(WeatherServiceCallback callback) { this.callback = callback;}

    /*public void refreshLocation(){
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL("http://api.wunderground.com/api/630b46099db0f87e/geolookup/q/autoip.json");

                    URLConnection connection = url.openConnection();

                    InputStream inputStream = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null){
                        result.append(line);
                    }
                    return result.toString();
                } catch (Exception e) {
                    error = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null){
                    callback.serviceFailure(error);
                    return;
                }

                try {
                    JSONObject data = new JSONObject(s);

                    //if the location does not exist, there is JSONObjest error
                    JSONObject ifError = data.optJSONObject("error");
                    if (ifError != null){
                        callback.serviceFailure(new LocationWeatherException(location+": Invalid Location"));
                        return;
                    }
                    Location l = new Location();
                    l.populate(data.optJSONObject("location"));
                    location = l;
                    callback.serviceGeolookupSuccess(location);
                } catch (JSONException e) {
                    callback.serviceFailure(e);
                }
            }
        }.execute();
    }*/

    public void refreshConditions(final String lat, final String lon){
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) { //not running on the UI thread

                String address = "http://api.wunderground.com/api/630b46099db0f87e/conditions/q/"+lat+","+lon+".json";

                String endpoint = String.format(address);

                try {
                    URL url = new URL(endpoint);

                    URLConnection connection = url.openConnection();

                    InputStream inputStream = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null){
                        result.append(line);
                    }
                    return result.toString();
                } catch (Exception e) {
                    error = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null){
                    callback.serviceFailure(error);
                    return;
                }

                try {
                    JSONObject data = new JSONObject(s);

                    //if the location does not exist, there is error
                    JSONObject ifError = data.optJSONObject("error");
                    if (ifError != null){
                        callback.serviceFailure(new LocationWeatherException(location+": Invalid Location"));
                        return;
                    }

                    Current_observation current_observation = new Current_observation();
                    current_observation.populate(data.optJSONObject("current_observation"));

                    callback.serviceConditionsSuccess(current_observation);
                } catch (JSONException e) {
                    callback.serviceFailure(e);
                }
            }
        }.execute();
    }

    public void refreshHourly(final String lat, final String lon){
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) { //not running on the UI thread

                String address = "http://api.wunderground.com/api/630b46099db0f87e/hourly/q/"+lat+","+lon+".json";

                String endpoint = String.format(address);

                try {
                    URL url = new URL(endpoint);

                    URLConnection connection = url.openConnection();

                    InputStream inputStream = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null){
                        result.append(line);
                    }
                    return result.toString();
                } catch (Exception e) {
                    error = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null){
                    callback.serviceFailure(error);
                    return;
                }

                try {
                    JSONObject data = new JSONObject(s);

                    //if the location does not exist, there is error
                    JSONObject ifError = data.optJSONObject("error");
                    if (ifError != null){
                        callback.serviceFailure(new LocationWeatherException(location+": Invalid Location"));
                        return;
                    }

                    Hourly hourly = new Hourly();
                    hourly.populate(data.optJSONArray("hourly_forecast"));

                    callback.serviceHourlySuccess(hourly);
                } catch (JSONException e) {
                    callback.serviceFailure(e);
                }
            }
        }.execute();
    }

    public void refreshForecast(final String lat, final String lon){
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) { //not running on the UI thread

                String address = "http://api.wunderground.com/api/630b46099db0f87e/forecast/q/"+lat+","+lon+".json";

                String endpoint = String.format(address);

                try {
                    URL url = new URL(endpoint);

                    URLConnection connection = url.openConnection();

                    InputStream inputStream = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null){
                        result.append(line);
                    }
                    return result.toString();
                } catch (Exception e) {
                    error = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null){
                    callback.serviceFailure(error);
                    return;
                }

                try {
                    JSONObject data = new JSONObject(s);

                    //if the location does not exist, there is error
                    JSONObject ifError = data.optJSONObject("error");
                    if (ifError != null){
                        callback.serviceFailure(new LocationWeatherException(location+": Invalid Location"));
                        return;
                    }

                    Forecast forecast = new Forecast();
                    forecast.populate(data.optJSONObject("forecast").optJSONObject("simpleforecast").optJSONArray("forecastday"));

                    callback.serviceForecastSuccess(forecast);
                } catch (JSONException e) {
                    callback.serviceFailure(e);
                }
            }
        }.execute();
    }

    public class LocationWeatherException extends Exception{
        public LocationWeatherException(String detailMessage) {
            super(detailMessage);
        }
    }
}
